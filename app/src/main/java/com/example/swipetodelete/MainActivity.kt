package com.example.swipetodelete

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.helper.ItemTouchHelper
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {
    private lateinit var simpleListAdapter: SimpleListAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val items = mutableListOf(1, 2, 3, 4, 5, 6, 7, 8, 9, 10)
        simpleListAdapter = SimpleListAdapter(this, items)

        rvSimpleList.layoutManager = LinearLayoutManager(this)
        rvSimpleList.adapter = simpleListAdapter

        val color = getColor(R.color.item_list_delete_background)
        val swipeToDeleteCallback = ItemTouchHelper(SwipeToDeleteCallback(simpleListAdapter, color))
        swipeToDeleteCallback.attachToRecyclerView(rvSimpleList)
    }
}
