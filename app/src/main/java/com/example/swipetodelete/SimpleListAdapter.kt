package com.example.swipetodelete

import android.app.Activity
import android.content.Context
import android.support.design.widget.Snackbar
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import kotlinx.android.synthetic.main.item_simple_list.view.*

class SimpleListAdapter(private val context: Context, private val items: MutableList<Int>) :
    RecyclerView.Adapter<SimpleListAdapter.ViewHolder>() {

    private var removedItem = 0
    private var removedItemPosition = 0

    override fun getItemCount(): Int {
        return items.size
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(LayoutInflater.from(context).inflate(R.layout.item_simple_list, parent, false))
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.tvItem.text = "Item ${items[position]}"
    }

    // Удаление элемента
    fun removeItem(position: Int) {
        removedItem = items[position]
        removedItemPosition = position

        items.removeAt(position)
        notifyItemRemoved(position)

        showUndoSnackbar()
    }

    // Уведомление об удалении
    private fun showUndoSnackbar() {
        val view = (context as Activity).findViewById<View>(R.id.listContainer)

        Snackbar.make(
            view,
            R.string.snackbar_undo_label,
            Snackbar.LENGTH_LONG
        ).setAction(R.string.snackbar_undo_button) {
            undoDelete()
        }.show()
    }

    // Отмена удаления
    private fun undoDelete() {
        items.add(removedItemPosition, removedItem)
        notifyItemInserted(removedItemPosition)
    }

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val tvItem = view.tvItem!!
    }
}